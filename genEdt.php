<?php

class Seance {
    private $nomSeance, $jour, $heureDebut, $heureFin, $salle;

    public function __construct($nomSeance, $jour, $heureDebut, $heureFin, $salle) {

        $this->nomSeance = $nomSeance;
        $this->jour = $jour;
        $this->heureDebut = $heureDebut;
        $this->heureFin = $heureFin;
        $this->salle = $salle;

    }

    public function getNomSeance() {
        return $this->nomSeance;
    }
    public function getJour() {
        return $this->jour;
    }
    public function getHeureDebut() {
        return $this->heureDebut;
    }
    public function getHeureFin() {
        return $this->heureFin;
    }
    public function getSalle() {
        return $this->salle;
    }

}


/* Reçoit une ligne du fichier des UEs et retourne */
function parseLigne($ligne) {
    $champs = split(" ", trim($ligne));
    return 
        new Seance($champs[0], $champs[1], $champs[2], 
        $champs[3], (isset($champs[4]) ? $champs[4] : ""));
}

function stringToMinutes($str) {
    $res = 0;
    if(preg_match("@(\d\d?)h(\d\d)?@", $str, $resRE)) {
        if(isset($resRE[2]))
            $res = $resRE[1] * 60 + $resRE[2];
        else
            $res = $resRE[1] * 60;
    } else {
        echo "$str not recognise\n";
    }

    return $res;
}

function dayOfWeek($day) {
    switch($day) {
    case "lundi": return 0;
    case "mardi": return 1;
    case "mercredi": return 2;
    case "jeudi": return 3;
    case "vendredi": return 4;
    }
}

$colors = array();
$ues = array();
$colors = array(
    0 => "#FF3333", 1 => "#CCFF00", 2 => "#33CC00", 3 => "#99FFFF",
    4 => "#CC00CC", 5 => "#FFFF99");

$fichier = file("listeUEs.txt");

$seances = array();
foreach($fichier as $ligne) {
    /* Si la ligne n'est pas un commentaire, on la parse */
    if($ligne[0] != '#' && strlen($ligne) > 1) {
        $seance = parseLigne($ligne);
        /* Si nouvelle ue, on affecte une couleur */
        if(!isset($ues[$seance->getNomSeance()])) {
            $ues[$seance->getNomSeance()] = $colors[count($ues)];
        }

        $seances[] = $seance;
    }
}

$coef = 1;
$hauteur = 55;
$marge = 15;
$paddingSeances = 200;          /* 8h30 sera à 200px du bord */
$largeurCelluleJour = 100;
$fichier = "
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML Basic 1.1//EN' 'http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd'>
<html xml:lang='fr' lang='fr' xmlns='http://www.w3.org/1999/xhtml'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <title> Emploi du temps </title>
    <style>
h1 {
  display : inline-block;
  width : ".(stringToMinutes("18h")-stringToMinutes("8h30")+$paddingSeances)*$coef."px;
  text-align : center;
  margin: 0 0 10px;
}
.celluleHeure, .celluleJour, .ligne, .colonne, .seance {
  position : absolute;
}
.celluleHeure, .celluleJour {
  z-index : 2;
}
.seance, .celluleHeure, .celluleJour {
  vertical-align : middle;
  line-height : ".$hauteur."px;
  text-align : center;
  border-radius: 10px;
  z-index : 2;
}
.ligne, .colonne {
  background-color : #DEDEDE;
  border-radius : 10px;
  z-index : 1;
}
.colonne {
  display : none;
}
.seance div {
  font-size : 130%;
  line-height : 30px;
  padding-top : 5px;
}
.seance div + div {
  font-size : 90%;
  line-height : 20px;
  padding-top : 0px;
}
    </style>
  </head>
  <body><div>
<!-- .................................. Corps de la page ........................................... -->
    <h1> Emploi du temps </h1>
      <div style='position:relative;'>
";

/* Générer les balises pour les jours */
$jours = array("Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi");
$width = $largeurCelluleJour;
$height = $hauteur;
/* /\* Pour top : on ajoute hauteur pour la ligne des heures, et on soustrait, pour */
/*  * le cas initial, hauteur + marge *\/ */
/* $top = $hauteur - $hauteur - $marge; */
$top = 0;
$left = 0;
$margeLigne = $marge/3;
$widthLigne = $coef * (stringToMinutes("18h") - stringToMinutes("8h30") + $paddingSeances + $margeLigne);
$heightLigne = $hauteur + (2*$margeLigne);
$leftLigne = 0;
foreach($jours as $jour) {
    $top = $top + $hauteur + $marge;
    $topLigne = $top - $margeLigne;
    $fichier .= "<div class='celluleJour' style='width:".$width."px; height:".$height."px; top:".$top."px; left:".$left."px;'>".$jour."</div>
<div class='ligne' style='width:".$widthLigne."px; height:".$heightLigne."px; top:".$topLigne."px; left:".$leftLigne."px;'></div>\n";
}
$fichier .= "\n";

/* Générer les balises pour les heures */
$heures = array("8h30 10h30", "10h45 12h45", "13h45 15h45", "16h 18h");
$top = 0;
$height = $hauteur;
$margeColonne = $marge/3;
/* Pour heightColonne : 5 jours + ligne des heures fois la hauteur d'une colonne
 * plus une marge en bas */
$heightColonne = 6 * ($hauteur + $marge) + $margeColonne - $marge;
$topColonne = 0;
foreach($heures as $heure) {
    $bornes = split(" ", $heure);
    $width = $coef * (stringToMinutes($bornes[1]) - stringToMinutes($bornes[0]));
    $widthColonne = $width + (2*$margeColonne);
    $left = ($coef * stringToMinutes($bornes[0])) - stringToMinutes("8h30") + $paddingSeances;
    $leftColonne = $left - $margeColonne;

    $fichier .= "<div class='celluleHeure' style='width:".$width."px; height:".$height."px; top:".$top."px; left:".$left."px;'>".$heure."</div>
<div class='colonne' style='width:".$widthColonne."px; height:".$heightColonne."px; top:".$topColonne."px; left:".$leftColonne."px;'></div>\n";
}
$fichier .= "\n";


/* Générer les balises pour les séances */
foreach($seances as $seance) {
    $width = $coef * (stringToMinutes($seance->getHeureFin()) - 
        stringToMinutes($seance->getHeureDebut()));
    $height = $hauteur;
    /* Pour top : +1 pour la ligne des heures */
    $top = ($hauteur+$marge) * (dayOfWeek($seance->getJour()) + 1);
    $left = ($coef * stringToMinutes($seance->getHeureDebut())) - stringToMinutes("8h30") + $paddingSeances;

    $fichier .= "<div class='seance' style='background-color:".$ues[$seance->getNomSeance()]."; width:".$width."px; height:".$height."px; top:".$top."px; left:".$left."px;'><div>".$seance->getNomSeance()."</div><div>".$seance->getSalle()."</div></div>\n";

}

$fichier .= "
      </div>
<!-- ............................................................................................... -->
  </div></body>
</html>
";

echo $fichier;

?>